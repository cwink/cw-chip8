#ifndef C8_FILE_H
#define C8_FILE_H

#include "c8_types.h"

#include <stdio.h>

C8_BOOL c8_file_open_binary (FILE **file, const C8_CHAR *const filename);

C8_BOOL c8_file_close (FILE **file);

C8_BOOL c8_file_get (FILE *file, C8_S32 *value);

#endif /* C8_FILE_H */
