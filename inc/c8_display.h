#ifndef C8_DISPLAY_H
#define C8_DISPLAY_H

#include "c8_types.h"

#define C8_DISPLAY_FONT_FILE "dat/free_mono.ttf"

#define C8_DISPLAY_FONT_SIZE 16

#define C8_DISPLAY_FONT_COLOR C8_DISPLAY_RGBA (255, 0, 0, 255)

#define C8_DISPLAY_RGBA(RED, GREEN, BLUE, ALPHA)                              \
  ((((C8_U32)RED << 24) & 0xFF000000) | (((C8_U32)GREEN << 16) & 0x00FF0000)  \
   | (((C8_U32)BLUE << 8) & 0x0000FF00) | ((C8_U32)ALPHA & 0x000000FF))

typedef struct c8_display_t C8_DISPLAY;

typedef struct c8_display_render_t C8_DISPLAY_RENDER;

typedef struct c8_display_dimensions_t
{
  C8_SIZE x;

  C8_SIZE y;

  C8_SIZE width;

  C8_SIZE height;
} C8_DISPLAY_DIMENSIONS;

C8_DISPLAY *c8_display_create (const C8_CHAR *const title, const C8_SIZE width,
                               const C8_SIZE height, const C8_SIZE res_width,
                               const C8_SIZE res_height,
                               const C8_CHAR *font_file,
                               const C8_SIZE font_size);

C8_VOID c8_display_destroy (C8_DISPLAY *const display);

C8_DISPLAY_RENDER *c8_display_render_create (C8_DISPLAY *const display,
                                             const C8_BOOL streaming);

C8_VOID c8_display_render_destroy (C8_DISPLAY_RENDER *const render);

C8_VOID c8_display_render_dimensions (C8_DISPLAY_RENDER *const render,
                                      C8_DISPLAY_DIMENSIONS *dimensions);

C8_BOOL c8_display_render_graphics (C8_DISPLAY_RENDER *const render,
                                    const C8_U32 *const graphics);

C8_BOOL c8_display_render_text (C8_DISPLAY *const display,
                                C8_DISPLAY_RENDER *const render,
                                const C8_SIZE x, const C8_SIZE y,
                                const C8_U32 color_index,
                                const C8_CHAR *const text);

C8_BOOL c8_display_render (C8_DISPLAY *const display,
                           C8_DISPLAY_RENDER *const render);

C8_VOID c8_display_update (C8_DISPLAY *const display);

C8_VOID c8_display_beep (C8_DISPLAY *const display, const C8_BOOL on);

#endif /* C8_DISPLAY_H */
