#ifndef C8_MACHINE_H
#define C8_MACHINE_H

#include "c8_display.h"
#include "c8_types.h"

#define C8_MACHINE_MEMORY_SIZE 4096

#define C8_MACHINE_REGISTER_SIZE 17

#define C8_MACHINE_STACK_SIZE 16

#define C8_MACHINE_KEY_SIZE 16

#define C8_MACHINE_INTERPRETER_START_ADDRESS 0x0000

#define C8_MACHINE_INTERPRETER_END_ADDRESS 0x01FF

#define C8_MACHINE_FONT_START_ADDRESS 0x0000

#define C8_MACHINE_FONT_END_ADDRESS 0x00A0

#define C8_MACHINE_PROGRAM_START_ADDRESS 0x0200

#define C8_MACHINE_PROGRAM_END_ADDRESS 0x0FFF

#define C8_MACHINE_GRAPHICS_WIDTH 64

#define C8_MACHINE_GRAPHICS_HEIGHT 32

#define C8_MACHINE_GRAPHICS_SIZE                                              \
  (C8_MACHINE_GRAPHICS_WIDTH * C8_MACHINE_GRAPHICS_HEIGHT)

#define C8_MACHINE_FONT_SET_SIZE 80

#define C8_MACHINE_RGB_OFF C8_DISPLAY_RGBA (0, 0, 0, 0)

#define C8_MACHINE_RGB_ON C8_DISPLAY_RGBA (255, 255, 255, 255)

typedef struct c8_machine_t
{
  C8_U16 index_register;

  C8_U16 program_counter;

  C8_U16 stack[C8_MACHINE_STACK_SIZE];

  C8_U16 stack_pointer;

  C8_BYTE memory[C8_MACHINE_MEMORY_SIZE];

  C8_BYTE registers[C8_MACHINE_REGISTER_SIZE];

  C8_BYTE delay_timer;

  C8_BYTE sound_timer;

  C8_U8 _padding[3];

  C8_U32 graphics[C8_MACHINE_GRAPHICS_SIZE];

  C8_BOOL key_state[C8_MACHINE_KEY_SIZE];

  C8_BOOL key_wait;
} C8_MACHINE;

C8_VOID c8_machine_initialize (C8_MACHINE *machine);

C8_BOOL c8_machine_load_code (C8_MACHINE *machine,
                              const C8_CHAR *const filename);

C8_OPCODE c8_machine_get_next_opcode (C8_MACHINE *machine);

C8_VOID c8_machine_process_timers (C8_MACHINE *machine);

C8_BOOL c8_machine_process_opcode (C8_MACHINE *machine,
                                   const C8_OPCODE opcode);

#endif /* C8_MACHINE_H */
