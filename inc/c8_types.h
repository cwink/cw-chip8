#ifndef C8_TYPES_H
#define C8_TYPES_H

#include <stdlib.h>

#define C8_CHAR char

#define C8_VOID void

#define C8_S8 char

#define C8_U8 unsigned char

#define C8_S16 short

#define C8_U16 unsigned short

#define C8_S32 int

#define C8_U32 unsigned int

#define C8_S64 long

#define C8_U64 unsigned long

#define C8_SIZE size_t

#define C8_R32 float

#define C8_R64 double

#define C8_BYTE C8_U8

typedef enum c8_bool_t
{
  C8_BOOL_FALSE = 0,
  C8_BOOL_TRUE = 1
} C8_BOOL;

#define C8_OPCODE C8_U16

#endif /* C8_TYPES_H */
