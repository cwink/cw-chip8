#ifndef C8_ERROR_H
#define C8_ERROR_H

#include "c8_types.h"

#define C8_ERROR_MESSAGE_SIZE 4096

#define C8_ERROR_WRITE(FORMAT) c8_error_write (__FILE__, __LINE__, FORMAT)

#define C8_ERROR_WRITE_1(FORMAT, A)                                           \
  c8_error_write (__FILE__, __LINE__, FORMAT, A)

#define C8_ERROR_WRITE_2(FORMAT, A, B)                                        \
  c8_error_write (__FILE__, __LINE__, FORMAT, A, B)

#define C8_ERROR_WRITE_3(FORMAT, A, B, C)                                     \
  c8_error_write (__FILE__, __LINE__, FORMAT, A, B, C)

#define C8_ERROR_WRITE_4(FORMAT, A, B, C, D)                                  \
  c8_error_write (__FILE__, __LINE__, FORMAT, A, B, C, D)

#define C8_ERROR_CHECK(CONDITION, RETURN, FORMAT)                             \
  do                                                                          \
    {                                                                         \
      if (!(CONDITION))                                                       \
        {                                                                     \
          C8_ERROR_WRITE (FORMAT);                                            \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define C8_ERROR_CHECK_1(CONDITION, RETURN, FORMAT, A)                        \
  do                                                                          \
    {                                                                         \
      if (!(CONDITION))                                                       \
        {                                                                     \
          C8_ERROR_WRITE_1 (FORMAT, A);                                       \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define C8_ERROR_CHECK_2(CONDITION, RETURN, FORMAT, A, B)                     \
  do                                                                          \
    {                                                                         \
      if (!(CONDITION))                                                       \
        {                                                                     \
          C8_ERROR_WRITE_2 (FORMAT, A, B);                                    \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define C8_ERROR_CHECK_3(CONDITION, RETURN, FORMAT, A, B, C)                  \
  do                                                                          \
    {                                                                         \
      if (!(CONDITION))                                                       \
        {                                                                     \
          C8_ERROR_WRITE_3 (FORMAT, A, B, C);                                 \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

#define C8_ERROR_CHECK_4(CONDITION, RETURN, FORMAT, A, B, C, D)               \
  do                                                                          \
    {                                                                         \
      if (!(CONDITION))                                                       \
        {                                                                     \
          C8_ERROR_WRITE_4 (FORMAT, A, B, C, D);                              \
                                                                              \
          return RETURN;                                                      \
        }                                                                     \
    }                                                                         \
  while (0)

C8_VOID c8_error_write (const C8_CHAR *const filename, const C8_SIZE line,
                        const C8_CHAR *const format, ...);

#endif /* C8_ERROR_H */
