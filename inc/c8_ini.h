#ifndef C8_INI_H
#define C8_INI_H

#include "c8_types.h"

#define C8_INI_MAX_DATA_SIZE 32

#define C8_INI_MAX_BUFFER_SIZE 256

typedef struct c8_ini_t C8_INI;

C8_INI *c8_ini_create (const C8_CHAR *const file);

C8_VOID c8_ini_destroy (C8_INI **ini);

C8_BOOL c8_ini_set (C8_INI *const ini, C8_CHAR *const key,
                    C8_CHAR *const value);

C8_CHAR *c8_ini_get (const C8_INI *const ini, const C8_CHAR *const key);

#endif /* C8_INI_H */
