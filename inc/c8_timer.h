#ifndef C8_TIMER_H
#define C8_TIMER_H

#include "c8_machine.h"

#define C8_TIMER_DELTA_TIME 0.0167

typedef struct c8_timer_t
{
  C8_R64 time;

  C8_R64 delta_time;

  C8_R64 current_time;

  C8_R64 accumulator;
} C8_TIMER;

C8_VOID c8_timer_initialize (C8_TIMER *timer);

C8_VOID c8_timer_process (C8_TIMER *timer);

C8_VOID c8_timer_accumulate (C8_TIMER *timer);

#endif /* C8_TIMER_H */
