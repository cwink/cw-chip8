#ifndef C8_EVENT_H
#define C8_EVENT_H

#include "c8_machine.h"

C8_BOOL c8_events_process (C8_MACHINE *machine, C8_BOOL *debug, C8_BOOL *step,
                           C8_CHAR keys[C8_MACHINE_KEY_SIZE][32],
                           C8_CHAR debug_key[32], C8_CHAR step_key[32],
                           C8_CHAR quit_key[32]);

C8_VOID c8_events_reset (C8_MACHINE *machine);

C8_BOOL c8_events_valid_key (const C8_CHAR *const key_string);

#endif /* C8_EVENT_H */
