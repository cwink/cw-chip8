#include "../inc/c8_ini.h"
#include "../inc/c8_error.h"

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

typedef struct c8_ini_pair_t
{
  C8_CHAR *key;

  C8_CHAR *value;

  struct c8_ini_pair_t *next;
} C8_INI_PAIR;

struct c8_ini_t
{
  C8_INI_PAIR *data[C8_INI_MAX_DATA_SIZE];
};

static C8_U64
c8_ini_hasher (const C8_CHAR *const key)
{
  C8_SIZE index = 0;

  C8_U64 hash = 14695981039346656037UL;

  assert (key != NULL);

  for (index = 0; index < strlen (key); ++index)
    {
      hash ^= (C8_U64)key[index];

      hash *= 1099511628211UL;
    }

  return hash;
}

static C8_INI_PAIR *
c8_ini_pair_create (C8_CHAR *const key, C8_CHAR *const value)
{
  C8_INI_PAIR *pair = NULL;

  assert (key != NULL);

  assert (value != NULL);

  pair = malloc (sizeof (C8_INI_PAIR));

  C8_ERROR_CHECK (pair != NULL, NULL,
                  "failed to create ini pair, allocation of pair failed");

  pair->key = key;

  pair->value = value;

  pair->next = NULL;

  return pair;
}

static C8_VOID
c8_ini_pair_destroy (C8_INI_PAIR *pair)
{
  assert (pair != NULL);

  assert (pair->key != NULL);

  assert (pair->value != NULL);

  free (pair->key);

  free (pair->value);

  free (pair);
}

static C8_CHAR *
c8_ini_duplicate_string (const C8_CHAR *const string)
{
  C8_CHAR *new_string = NULL;

  assert (string != NULL);

  new_string = malloc ((sizeof (C8_CHAR) * strlen (string)) + 1);

  C8_ERROR_CHECK (
      new_string != NULL, NULL,
      "failed to duplicate string, allocation of new string failed");

  strcpy (new_string, string);

  return new_string;
}

static C8_VOID
c8_ini_remove_newline (C8_CHAR *const string)
{
  C8_SIZE index = 0;

  assert (string != NULL);

  for (index = 0; index < strlen (string); ++index)
    {
      if (string[index] == '\n' || string[index] == '\r')
        {
          string[index] = '\0';

          return;
        }
    }
}

static C8_BOOL
c8_ini_trim_string (C8_CHAR *const string)
{
  C8_S64 index = 0;

  C8_S64 index_1 = 0;

  C8_S64 index_2 = 0;

  C8_CHAR *buffer = NULL;

  C8_SIZE length = strlen (string);

  assert (string != NULL);

  if (length == 0)
    {
      return C8_BOOL_TRUE;
    }

  buffer = malloc ((sizeof (C8_CHAR) * length) + 1);

  C8_ERROR_CHECK (buffer != NULL, C8_BOOL_FALSE,
                  "failed to trim string, allocation of buffer failed");

  for (index_1 = 0; index_1 <= (C8_S64)length; ++index_1)
    {
      if (isspace (string[index_1]) == 0)
        {
          break;
        }
    }

  for (index_2 = (C8_S64)length - 1; index_2 >= 0; --index_2)
    {
      if (isspace (string[index_2]) == 0)
        {
          break;
        }
    }

  ++index_2;

  for (index = 0; index < index_2 - index_1; ++index)
    {
      buffer[index] = string[index_1 + index];
    }

  buffer[index] = '\0';

  strcpy (string, buffer);

  free (buffer);

  return C8_BOOL_TRUE;
}

static C8_SIZE
c8_ini_count_equals (const C8_CHAR *const string)
{
  C8_SIZE index = 0;

  C8_SIZE count = 0;

  assert (string != NULL);

  for (index = 0; index < strlen (string); ++index)
    {
      if (string[index] == '=')
        {
          ++count;
        }
    }

  return count;
}

C8_INI *
c8_ini_create (const C8_CHAR *const file)
{
  FILE *fs = NULL;

  C8_CHAR buffer[C8_INI_MAX_BUFFER_SIZE + 1];

  C8_SIZE index = 0;

  C8_INI *ini = NULL;

  assert (file != NULL);

  fs = fopen (file, "r");

  C8_ERROR_CHECK_1 (fs != NULL, NULL,
                    "failed to create ini, failed to open file '%s'", file);

  ini = malloc (sizeof (C8_INI));

  if (ini == NULL)
    {
      C8_ERROR_WRITE ("failed to create ini, allocation of ini failed");

      goto c8_ini_create_failure;
    }

  for (index = 0; index < C8_INI_MAX_DATA_SIZE; ++index)
    {
      ini->data[index] = NULL;
    }

  while (fgets (buffer, C8_INI_MAX_BUFFER_SIZE, fs) != NULL)
    {
      C8_CHAR *key = NULL;

      C8_CHAR *value = NULL;

      C8_CHAR *ptr = NULL;

      if (ferror (fs) != 0)
        {
          C8_ERROR_WRITE_1 ("failed to create ini, couldn't get string from "
                            "file stream for file '%s'",
                            file);

          goto c8_ini_create_failure;
        }

      c8_ini_remove_newline (buffer);

      if (c8_ini_trim_string (buffer) != C8_BOOL_TRUE)
        {
          C8_ERROR_WRITE_1 ("failed to create ini, unable to trim string '%s'",
                            buffer);

          free (key);

          goto c8_ini_create_failure;
        }

      if (buffer[0] == ';' || strcmp (buffer, "") == 0)
        {
          continue;
        }

      if (c8_ini_count_equals (buffer) != 1)
        {
          C8_ERROR_WRITE_1 ("failed to create ini, invalid number of '=' "
                            "characters in string '%s'",
                            buffer);

          goto c8_ini_create_failure;
        }

      ptr = strtok (buffer, "=");

      key = c8_ini_duplicate_string (ptr);

      if (key == NULL)
        {
          C8_ERROR_WRITE_1 (
              "failed to create ini, failed to duplicate key string '%s'",
              ptr);

          goto c8_ini_create_failure;
        }

      if (c8_ini_trim_string (key) != C8_BOOL_TRUE)
        {
          C8_ERROR_WRITE_1 (
              "failed to create ini, unable to trim key string '%s'", key);

          free (key);

          goto c8_ini_create_failure;
        }

      ptr = strtok (NULL, "=");

      value = c8_ini_duplicate_string (ptr);

      if (value == NULL)
        {
          C8_ERROR_WRITE_1 (
              "failed to create ini, failed to duplicate value string '%s'",
              ptr);

          free (key);

          goto c8_ini_create_failure;
        }

      if (c8_ini_trim_string (value) != C8_BOOL_TRUE)
        {
          C8_ERROR_WRITE_1 (
              "failed to create ini, unable to trim value string '%s'", key);

          free (key);

          free (value);

          goto c8_ini_create_failure;
        }

      if (c8_ini_set (ini, key, value) != C8_BOOL_TRUE)
        {
          C8_ERROR_WRITE_1 ("failed to create ini, failed to set key/value "
                            "pair for file '%s'",
                            file);

          free (key);

          free (value);

          goto c8_ini_create_failure;
        }
    }

  fclose (fs);

  return ini;

c8_ini_create_failure:
  if (fs != NULL)
    {
      fclose (fs);
    }

  if (ini != NULL)
    {
      c8_ini_destroy (&ini);
    }

  return NULL;
}

C8_VOID
c8_ini_destroy (C8_INI **ini)
{
  C8_SIZE index = 0;

  assert (ini != NULL);

  assert (*ini != NULL);

  for (index = 0; index < C8_INI_MAX_DATA_SIZE; ++index)
    {
      if ((*ini)->data[index] != NULL)
        {
          C8_INI_PAIR *ptr = NULL;

          for (ptr = (*ini)->data[index]; ptr != NULL;)
            {
              C8_INI_PAIR *next = ptr->next;

              c8_ini_pair_destroy (ptr);

              ptr = next;
            }
        }
    }

  free (*ini);

  *ini = NULL;
}

C8_BOOL
c8_ini_set (C8_INI *const ini, C8_CHAR *const key, C8_CHAR *const value)
{
  C8_INI_PAIR *ptr = NULL;

  C8_U64 hash = 0UL;

  assert (ini != NULL);

  assert (key != NULL);

  assert (value != NULL);

  hash = c8_ini_hasher (key) % C8_INI_MAX_DATA_SIZE;

  if (ini->data[hash] == NULL)
    {
      ini->data[hash] = c8_ini_pair_create (key, value);

      C8_ERROR_CHECK (ini->data[hash] != NULL, C8_BOOL_FALSE,
                      "failed to set key in ini, pair creation failed");

      return C8_BOOL_TRUE;
    }

  if (strcmp (ini->data[hash]->key, key) == 0)
    {
      free (key);

      free (ini->data[hash]->value);

      ini->data[hash]->value = value;

      return C8_BOOL_TRUE;
    }

  for (ptr = ini->data[hash]; ptr->next != NULL; ptr = ptr->next)
    {
      if (strcmp (ptr->key, key) == 0)
        {
          free (key);

          free (ptr->value);

          ptr->value = value;

          return C8_BOOL_TRUE;
        }
    }

  ptr->next = c8_ini_pair_create (key, value);

  C8_ERROR_CHECK (ptr->next != NULL, C8_BOOL_FALSE,
                  "failed to set key in ini, pair creation failed");

  return C8_BOOL_TRUE;
}

C8_CHAR *
c8_ini_get (const C8_INI *const ini, const C8_CHAR *const key)
{
  C8_INI_PAIR *ptr = NULL;

  C8_U64 hash = 0UL;

  assert (ini != NULL);

  assert (key != NULL);

  hash = c8_ini_hasher (key) % C8_INI_MAX_DATA_SIZE;

  for (ptr = ini->data[hash]; ptr != NULL; ptr = ptr->next)
    {
      if (strcmp (ptr->key, key) == 0)
        {
          return ptr->value;
        }
    }

  C8_ERROR_WRITE ("failed to get key in ini, key not found");

  return NULL;
}
