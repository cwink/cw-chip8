#include "../inc/c8_display.h"
#include "../inc/c8_error.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <assert.h>
#include <math.h>

#define C8_DISPLAY_AUDIO_SAMPLE_RATE 44100

#define C8_DISPLAY_AUDIO_CHANNELS 2

#define C8_DISPLAY_AUDIO_SAMPLES 4096

#define C8_DISPLAY_AUDIO_PI 3.14159265359

struct c8_display_t
{
  SDL_Window *window;

  SDL_Renderer *renderer;

  TTF_Font *font;

  C8_SIZE resolution_width;

  C8_SIZE resolution_height;

  C8_SIZE width;

  C8_SIZE height;

  C8_R64 sound_timer;

  SDL_AudioDeviceID device_id;

  C8_U8 _padding[4];
};

struct c8_display_render_t
{
  SDL_Texture *texture;

  C8_SIZE width;

  C8_SIZE height;

  C8_SIZE x;

  C8_SIZE y;

  C8_BOOL streaming;

  C8_U8 padding[4];
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
static C8_VOID
c8_display_audio_callback (C8_VOID *user_data, C8_U8 *stream, C8_S32 length)
{
#pragma GCC diagnostic pop
  C8_SIZE index = 0;

  C8_SIZE size = (C8_SIZE) ((C8_SIZE)length / sizeof (C8_R32));

  C8_DISPLAY *display = (C8_DISPLAY *)user_data;

  C8_R32 *sm = (C8_R32 *)((C8_VOID *)stream);

  for (index = 0; index < size; index += 2)
    {
      sm[index] = (C8_R32) (0.03 * sin (display->sound_timer));

      sm[index + 1] = (C8_R32) (0.03 * sin (display->sound_timer));

      display->sound_timer += 523.2511 * 2.0 * C8_DISPLAY_AUDIO_PI
                              / (C8_R64)C8_DISPLAY_AUDIO_SAMPLE_RATE;

      if (display->sound_timer >= 2.0 * C8_DISPLAY_AUDIO_PI)
        {
          display->sound_timer -= 2.0 * C8_DISPLAY_AUDIO_PI;
        }
    }
}

C8_DISPLAY *
c8_display_create (const C8_CHAR *const title, const C8_SIZE width,
                   const C8_SIZE height, const C8_SIZE res_width,
                   const C8_SIZE res_height, const C8_CHAR *font_file,
                   const C8_SIZE font_size)
{
  C8_DISPLAY *display = NULL;

  SDL_AudioSpec want;

  SDL_AudioSpec have;

  assert (title != NULL);

  display = malloc (sizeof (C8_DISPLAY));

  display->window = NULL;

  display->renderer = NULL;

  display->font = NULL;

  C8_ERROR_CHECK (display != NULL, NULL,
                  "failed to create display, allocation of display failed");

  if (SDL_Init (SDL_INIT_EVERYTHING) < 0)
    {
      C8_ERROR_WRITE (
          "failed to create display, initialization of SDL2 failed");

      goto c8_display_create_failure;
    }

  display->window = SDL_CreateWindow (title, SDL_WINDOWPOS_CENTERED,
                                      SDL_WINDOWPOS_CENTERED, (C8_S32)width,
                                      (C8_S32)height, 0);

  if (display->window == NULL)
    {
      C8_ERROR_WRITE (
          "failed to create display, creation of SDL2 window failed");

      goto c8_display_create_failure;
    }

  display->renderer
      = SDL_CreateRenderer (display->window, -1, SDL_RENDERER_ACCELERATED);

  if (display->renderer == NULL)
    {
      C8_ERROR_WRITE (
          "failed to create display, creation of SDL2 renderer failed");

      goto c8_display_create_failure;
    }

  display->resolution_width = res_width;

  display->resolution_height = res_height;

  if (TTF_Init () == -1)
    {
      C8_ERROR_WRITE ("failed to create display, initialization of SDL2 "
                      "font library failed");

      goto c8_display_create_failure;
    }

  display->font
      = TTF_OpenFontRW (SDL_RWFromFile (font_file, "r"), 1, (C8_S32)font_size);

  if (display->font == NULL)
    {
      C8_ERROR_WRITE ("failed to create display, initialization of SDL2 "
                      "font library failed");

      goto c8_display_create_failure;
    }

  display->width = width;

  display->height = width;

  display->sound_timer = 0.0;

  want.freq = C8_DISPLAY_AUDIO_SAMPLE_RATE;

  want.format = AUDIO_F32;

  want.channels = C8_DISPLAY_AUDIO_CHANNELS;

  want.samples = C8_DISPLAY_AUDIO_SAMPLES;

  want.callback = c8_display_audio_callback;

  want.userdata = display;

  display->device_id = SDL_OpenAudioDevice (NULL, 0, &want, &have,
                                            SDL_AUDIO_ALLOW_FORMAT_CHANGE);

  if (display->device_id == 0)
    {
      C8_ERROR_WRITE (
          "failed to create display, open of audio device for SDL2 failed.");

      goto c8_display_create_failure;
    }

  return display;

c8_display_create_failure:
  SDL_CloseAudioDevice (display->device_id);

  if (display->font != NULL)
    {
      TTF_CloseFont (display->font);
    }

  if (display->renderer != NULL)
    {
      SDL_DestroyRenderer (display->renderer);
    }

  if (display->window != NULL)
    {
      SDL_DestroyWindow (display->window);
    }

  if (display != NULL)
    {
      free (display);
    }

  return NULL;
}

C8_VOID
c8_display_destroy (C8_DISPLAY *const display)
{
  assert (display != NULL);

  assert (display->window != NULL);

  assert (display->renderer != NULL);

  assert (display->font != NULL);

  TTF_CloseFont (display->font);

  SDL_DestroyRenderer (display->renderer);

  SDL_DestroyWindow (display->window);

  free (display);

  TTF_Quit ();

  SDL_Quit ();
}

C8_DISPLAY_RENDER *
c8_display_render_create (C8_DISPLAY *const display, const C8_BOOL streaming)
{
  C8_DISPLAY_RENDER *render = NULL;

  assert (display != NULL);

  assert (display->renderer != NULL);

  render = malloc (sizeof (C8_DISPLAY_RENDER));

  C8_ERROR_CHECK (render != NULL, NULL,
                  "failed to create render, could not allocate memory");

  if (streaming == C8_BOOL_TRUE)
    {
      render->texture = SDL_CreateTexture (
          display->renderer, SDL_PIXELFORMAT_RGB888,
          SDL_TEXTUREACCESS_STREAMING, (C8_S32)display->resolution_width,
          (C8_S32)display->resolution_height);

      if (render->texture == NULL)
        {
          C8_ERROR_WRITE ("failed to create render, could not create SDL2 "
                          "streaming texture");

          free (render);

          return NULL;
        }

      render->width = display->resolution_width;

      render->height = display->resolution_height;

      render->x = 0;

      render->y = 0;

      render->streaming = C8_BOOL_TRUE;

      return render;
    }

  render->texture = NULL;

  render->width = 0;

  render->height = 0;

  render->x = 0;

  render->y = 0;

  render->streaming = C8_BOOL_FALSE;

  return render;
}

C8_VOID
c8_display_render_destroy (C8_DISPLAY_RENDER *const render)
{
  assert (render != NULL);

  if (render->texture != NULL)
    {
      SDL_DestroyTexture (render->texture);
    }

  free (render);
}

C8_VOID
c8_display_render_dimensions (C8_DISPLAY_RENDER *const render,
                              C8_DISPLAY_DIMENSIONS *dimensions)
{
  assert (render != NULL);

  dimensions->x = render->x;

  dimensions->y = render->y;

  dimensions->width = render->width;

  dimensions->height = render->height;
}

C8_BOOL
c8_display_render_graphics (C8_DISPLAY_RENDER *const render,
                            const C8_U32 *const graphics)
{
  C8_SIZE index = 0;

  C8_U32 *pixels = NULL;

  C8_S32 pitch = 0;

  assert (render != NULL);

  assert (render->texture != NULL);

  assert (graphics != NULL);

  C8_ERROR_CHECK (
      render->streaming != C8_BOOL_FALSE, C8_BOOL_FALSE,
      "failed to render graphics, render texture is not a streaming texture");

  C8_ERROR_CHECK (
      SDL_LockTexture (render->texture, NULL, (C8_VOID *)&pixels, &pitch) == 0,
      C8_BOOL_FALSE, "failed to render graphics, could not lock SDL2 texture");

  for (index = 0; index < render->width * render->height; ++index)
    {
      pixels[index] = graphics[index];
    }

  SDL_UnlockTexture (render->texture);

  return C8_BOOL_TRUE;
}

C8_BOOL
c8_display_render_text (C8_DISPLAY *const display,
                        C8_DISPLAY_RENDER *const render, const C8_SIZE x,
                        const C8_SIZE y, const C8_U32 color_index,
                        const C8_CHAR *const text)
{
  C8_S32 width = 0;

  C8_S32 height = 0;

  SDL_Color color;

  SDL_Surface *surface = NULL;

  assert (display != NULL);

  assert (display->font != NULL);

  assert (display->renderer != NULL);

  assert (render != NULL);

  assert (text != NULL);

  C8_ERROR_CHECK (
      render->streaming != C8_BOOL_TRUE, C8_BOOL_FALSE,
      "failed to render text, render texture is a streaming texture");

  color.r = (C8_U8) ((color_index & 0xFF000000) >> 24);

  color.g = (C8_U8) ((color_index & 0x00FF0000) >> 16);

  color.b = (C8_U8) ((color_index & 0x0000FF00) >> 8);

  color.a = (C8_U8) (color_index & 0x000000FF);

  surface = TTF_RenderText_Solid (display->font, text, color);

  C8_ERROR_CHECK (surface != NULL, C8_BOOL_FALSE,
                  "failed to render text, could not create SDL2 surface");

  if (render->texture != NULL)
    {
      SDL_DestroyTexture (render->texture);
    }

  render->texture = SDL_CreateTextureFromSurface (display->renderer, surface);

  if (render->texture == NULL)
    {
      C8_ERROR_WRITE (
          "failed to render text, could not create texture from SDL2 surface");

      SDL_FreeSurface (surface);

      return C8_BOOL_FALSE;
    }

  SDL_FreeSurface (surface);

  C8_ERROR_CHECK (
      SDL_QueryTexture (render->texture, NULL, NULL, &width, &height) == 0,
      C8_BOOL_FALSE, "failed to render text, could not query text texture");

  render->x = x;

  render->y = y;

  render->width = (C8_SIZE)width;

  render->height = (C8_SIZE)height;

  return C8_BOOL_TRUE;
}

C8_BOOL
c8_display_render (C8_DISPLAY *const display, C8_DISPLAY_RENDER *const render)
{
  assert (display != NULL);

  assert (render != NULL);

  assert (render->texture != NULL);

  if (render->streaming == C8_BOOL_FALSE)
    {
      SDL_Rect destination;

      destination.x = (C8_S32)render->x;

      destination.y = (C8_S32)render->y;

      destination.w = (C8_S32)render->width;

      destination.h = (C8_S32)render->height;

      C8_ERROR_CHECK (SDL_RenderCopy (display->renderer, render->texture, NULL,
                                      &destination)
                          == 0,
                      C8_BOOL_FALSE,
                      "failed to render, could not copy texture into render");
    }
  else
    {
      C8_ERROR_CHECK (
          SDL_RenderCopy (display->renderer, render->texture, NULL, NULL) == 0,
          C8_BOOL_FALSE,
          "failed to render, could not copy texture into render");
    }

  return C8_BOOL_TRUE;
}

C8_VOID
c8_display_update (C8_DISPLAY *const display)
{
  assert (display != NULL);

  assert (display->renderer != NULL);

  SDL_RenderPresent (display->renderer);

  SDL_Delay (1);
}

C8_VOID
c8_display_beep (C8_DISPLAY *const display, const C8_BOOL on)
{
  assert (display != NULL);

  assert (display->device_id != 0);

  SDL_PauseAudioDevice (display->device_id, on == C8_BOOL_TRUE ? 0 : 1);
}
