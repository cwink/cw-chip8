#include "../inc/c8_timer.h"

#include <SDL2/SDL.h>

#include <assert.h>

C8_VOID
c8_timer_initialize (C8_TIMER *timer)
{
  assert (timer != NULL);

  timer->time = 0.0;

  timer->delta_time = C8_TIMER_DELTA_TIME;

  timer->current_time = SDL_GetTicks () * 0.001;

  timer->accumulator = 0;
}

C8_VOID
c8_timer_process (C8_TIMER *timer)
{
  C8_R64 new_time = 0;

  C8_R64 frame_time = 0;

  assert (timer != NULL);

  new_time = SDL_GetTicks () * 0.001;

  frame_time = new_time - timer->current_time;

  timer->current_time = new_time;

  timer->accumulator += frame_time;
}

C8_VOID
c8_timer_accumulate (C8_TIMER *timer)
{
  assert (timer != NULL);

  while (timer->accumulator >= timer->delta_time)
    {
      timer->accumulator -= timer->delta_time;

      timer->time += timer->delta_time;
    }
}
