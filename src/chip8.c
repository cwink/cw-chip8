#include "../inc/c8_config.h"
#include "../inc/c8_display.h"
#include "../inc/c8_error.h"
#include "../inc/c8_event.h"
#include "../inc/c8_ini.h"
#include "../inc/c8_machine.h"
#include "../inc/c8_timer.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

#define C8_CHIP8_TEXT_ARRAY_SIZE 18

#define C8_CHIP8_FONT_FILE_SIZE 4096

#define C8_CHIP8_ROM_FILE_SIZE 4096

#define C8_CHIP8_CONFIG_FILE_SIZE 4096

#define C8_CHIP8_WINDOW_TITLE_SIZE 512

typedef struct c8_chip8_chip_t
{
  C8_MACHINE machine;

  C8_DISPLAY *display;

  C8_TIMER timer;

  C8_DISPLAY_RENDER *graphics;

  C8_DISPLAY_RENDER *text[C8_CHIP8_TEXT_ARRAY_SIZE];

  C8_SIZE font_size;

  C8_SIZE window_width;

  C8_SIZE window_height;

  C8_SIZE debug_text_x;

  C8_SIZE debug_text_y;

  C8_U32 font_color;

  C8_CHAR window_title[C8_CHIP8_WINDOW_TITLE_SIZE];

  C8_CHAR font_file[C8_CHIP8_FONT_FILE_SIZE];

  C8_CHAR rom_file[C8_CHIP8_ROM_FILE_SIZE];

  C8_CHAR config_file[C8_CHIP8_CONFIG_FILE_SIZE];

  C8_CHAR keys[C8_MACHINE_KEY_SIZE][32];

  C8_CHAR debug_key[32];

  C8_CHAR step_key[32];

  C8_CHAR quit_key[32];

  C8_BOOL debug;

  C8_OPCODE opcode;

  C8_U8 _padding[6];
} C8_CHIP8_CHIP;

C8_U8 c8_chip8_parse_arguments (C8_CHIP8_CHIP *const chip, const int argc,
                                char *const argv[]);

C8_VOID c8_chip8_print_configuration (C8_VOID);

C8_BOOL c8_chip8_initialize_config (C8_CHIP8_CHIP *const chip);

C8_BOOL c8_chip8_initialize (C8_CHIP8_CHIP *const chip);

C8_BOOL c8_chip8_process (C8_CHIP8_CHIP *const chip, C8_BOOL *running);

C8_BOOL c8_chip8_generate_text (C8_CHIP8_CHIP *const chip, const C8_SIZE x,
                                const C8_SIZE y);

C8_BOOL c8_chip8_render (C8_CHIP8_CHIP *const chip);

C8_VOID c8_chip8_terminate (C8_CHIP8_CHIP *const chip);

int
main (int argc, char *argv[])
{
  C8_CHIP8_CHIP chip;

  C8_BOOL running = C8_BOOL_TRUE;

  C8_U8 arguments_result = 0;

  arguments_result = c8_chip8_parse_arguments (&chip, argc, argv);

  if (arguments_result == 0)
    {
      C8_ERROR_WRITE ("application failure, unable to parse arguments");

      goto main_exit_skip_failure;
    }

  if (arguments_result == 2)
    {
      goto main_exit_skip_success;
    }

  c8_chip8_print_configuration ();

  if (c8_chip8_initialize (&chip) != C8_BOOL_TRUE)
    {
      C8_ERROR_WRITE ("application failure, unable to initialize");

      goto main_exit_failure;
    }

  while (running == C8_BOOL_TRUE)
    {
      if (c8_chip8_process (&chip, &running) != C8_BOOL_TRUE)
        {
          C8_ERROR_WRITE ("application failure, unable to process");

          goto main_exit_failure;
        }

      if (c8_chip8_render (&chip) != C8_BOOL_TRUE)
        {
          C8_ERROR_WRITE ("application failure, unable to render");

          goto main_exit_failure;
        }

      c8_events_reset (&(chip.machine));
    }

  c8_chip8_terminate (&chip);

  return EXIT_SUCCESS;

main_exit_failure:
  c8_chip8_terminate (&chip);

  return EXIT_FAILURE;

main_exit_skip_success:
  return EXIT_SUCCESS;

main_exit_skip_failure:
  return EXIT_FAILURE;
}

C8_U8
c8_chip8_parse_arguments (C8_CHIP8_CHIP *const chip, const int argc,
                          char *const argv[])
{
  C8_SIZE index = 0;

  C8_BOOL got_r = C8_BOOL_FALSE;

  C8_BOOL got_c = C8_BOOL_FALSE;

  if (argc < 2 || argc > 6)
    {
      fprintf (stderr, "Usage: chip8 [-h] -c 'config_file' -r 'rom_file'\n");

      return 0;
    }

  for (index = 1; index < (C8_SIZE)argc; ++index)
    {
      if (strcmp (argv[index], "-c") == 0)
        {
          strcpy (chip->config_file, argv[index + 1]);

          ++index;

          got_c = C8_BOOL_TRUE;

          continue;
        }

      if (strcmp (argv[index], "-r") == 0)
        {
          strcpy (chip->rom_file, argv[index + 1]);

          ++index;

          got_r = C8_BOOL_TRUE;

          continue;
        }

      if (strcmp (argv[index], "-h") == 0)
        {
          fprintf (stdout,
                   "Usage: chip8 [-h] -c 'config_file' -r 'rom_file'\n");

          return 2;
        }
    }

  if (got_c != C8_BOOL_TRUE || got_r != C8_BOOL_TRUE)
    {
      fprintf (stderr, "Usage: chip8 [-h] -c 'config_file' -r 'rom_file'\n");

      return 0;
    }

  return 1;
}

C8_VOID c8_chip8_print_configuration (C8_VOID)
{
  fprintf (stdout, "Application Name: %s\n", C8_CONFIG_NAME);

  fprintf (stdout, "Application Version: %s\n", C8_CONFIG_VERSION);

  fprintf (stdout, "Compiler: %s\n", C8_CONFIG_COMPILER);

  fprintf (stdout, "Build Type: %s\n", C8_CONFIG_TYPE);

  fprintf (stdout, "Compiler Flags: %s\n", C8_CONFIG_CFLAGS);

  fprintf (stdout, "Linker Flags: %s\n", C8_CONFIG_LDFLAGS);

  fprintf (stdout, "Lib Flags: %s\n", C8_CONFIG_LIB);
}

static C8_VOID
c8_chip8_keyboard_copy (const C8_CHAR *const key_string, C8_CHAR buffer[32])
{
  C8_SIZE index = 0;

  assert (key_string != NULL);

  for (index = 0; index < 32; ++index)
    {
      if (key_string[index] == '\0')
        {
          buffer[index] = key_string[index];

          break;
        }

      buffer[index] = key_string[index];
    }
}

C8_BOOL
c8_chip8_initialize_config (C8_CHIP8_CHIP *const chip)
{
  C8_INI *ini = NULL;

  C8_CHAR *font_file_str = NULL;

  C8_CHAR *font_size_str = NULL;

  C8_CHAR *font_red_str = NULL;

  C8_CHAR *font_green_str = NULL;

  C8_CHAR *font_blue_str = NULL;

  C8_S32 font_red_int = 0;

  C8_S32 font_green_int = 0;

  C8_S32 font_blue_int = 0;

  C8_S32 font_size = 0;

  C8_CHAR *window_title_str = NULL;

  C8_CHAR *window_width_str = NULL;

  C8_CHAR *window_height_str = NULL;

  C8_CHAR *debug_x_str = NULL;

  C8_CHAR *debug_y_str = NULL;

  C8_S32 window_width_int = 0;

  C8_S32 window_height_int = 0;

  C8_S32 debug_x_int = 0;

  C8_S32 debug_y_int = 0;

  C8_CHAR *keyboard_0 = NULL;

  C8_CHAR *keyboard_1 = NULL;

  C8_CHAR *keyboard_2 = NULL;

  C8_CHAR *keyboard_3 = NULL;

  C8_CHAR *keyboard_4 = NULL;

  C8_CHAR *keyboard_5 = NULL;

  C8_CHAR *keyboard_6 = NULL;

  C8_CHAR *keyboard_7 = NULL;

  C8_CHAR *keyboard_8 = NULL;

  C8_CHAR *keyboard_9 = NULL;

  C8_CHAR *keyboard_A = NULL;

  C8_CHAR *keyboard_B = NULL;

  C8_CHAR *keyboard_C = NULL;

  C8_CHAR *keyboard_D = NULL;

  C8_CHAR *keyboard_E = NULL;

  C8_CHAR *keyboard_F = NULL;

  C8_CHAR *keyboard_debug = NULL;

  C8_CHAR *keyboard_step = NULL;

  C8_CHAR *keyboard_quit = NULL;

  assert (chip != NULL);

  ini = c8_ini_create (chip->config_file);

  if (ini == NULL)
    {
      C8_ERROR_WRITE_1 ("failed to initialize chip8 emulator config, loading "
                        "of ini file '%s' failed",
                        chip->config_file);

      goto c8_chip8_initialize_config_failure;
    }

  font_file_str = c8_ini_get (ini, "c8_display_font_file");

  if (font_file_str == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_display_font_file'");

      goto c8_chip8_initialize_config_failure;
    }

  strcpy (chip->font_file, font_file_str);

  font_size_str = c8_ini_get (ini, "c8_display_font_size");

  if (font_size_str == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_display_font_size'");

      goto c8_chip8_initialize_config_failure;
    }

  font_size = atoi (font_size_str);

  if (font_size == 0)
    {
      C8_ERROR_WRITE_1 ("failed to initialize chip8 emulator config, font "
                        "size '%s' is invalid or not set",
                        font_size_str);

      goto c8_chip8_initialize_config_failure;
    }

  chip->font_size = (C8_SIZE)font_size;

  font_red_str = c8_ini_get (ini, "c8_display_font_color_red");

  if (font_red_str == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_display_font_color_red'");

      goto c8_chip8_initialize_config_failure;
    }

  font_red_int = atoi (font_red_str);

  if (font_red_int < 0 || font_red_int > 255)
    {
      C8_ERROR_WRITE_1 ("failed to initialize chip8 emulator config, red font "
                        "color '%s' must be a valid number between 0 and 255",
                        font_red_str);

      goto c8_chip8_initialize_config_failure;
    }

  font_green_str = c8_ini_get (ini, "c8_display_font_color_blue");

  if (font_green_str == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_display_font_color_green'");

      goto c8_chip8_initialize_config_failure;
    }

  font_green_int = atoi (font_green_str);

  if (font_green_int < 0 || font_green_int > 255)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, green font color '%s' "
          "must be a valid number between 0 and 255",
          font_green_str);

      goto c8_chip8_initialize_config_failure;
    }

  font_blue_str = c8_ini_get (ini, "c8_display_font_color_green");

  if (font_blue_str == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_display_font_color_blue'");

      goto c8_chip8_initialize_config_failure;
    }

  font_blue_int = atoi (font_blue_str);

  if (font_blue_int < 0 || font_blue_int > 255)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, blue font color '%s' "
          "must be a valid number between 0 and 255",
          font_blue_str);

      goto c8_chip8_initialize_config_failure;
    }

  chip->font_color
      = C8_DISPLAY_RGBA (font_red_int, font_blue_int, font_green_int, 255);

  window_title_str = c8_ini_get (ini, "c8_chip8_window_title");

  if (window_title_str == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_chip8_window_title'");

      goto c8_chip8_initialize_config_failure;
    }

  strcpy (chip->window_title, window_title_str);

  window_width_str = c8_ini_get (ini, "c8_chip8_window_width");

  if (window_width_str == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_chip8_window_width'");

      goto c8_chip8_initialize_config_failure;
    }

  window_width_int = atoi (window_width_str);

  if (window_width_int <= 0)
    {
      C8_ERROR_WRITE (
          "failed to initialize chip8 emulator config, window width either is "
          "not a number greater than zero or not a number at all");

      goto c8_chip8_initialize_config_failure;
    }

  chip->window_width = (C8_SIZE)window_width_int;

  window_height_str = c8_ini_get (ini, "c8_chip8_window_height");

  if (window_height_str == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_chip8_window_height'");

      goto c8_chip8_initialize_config_failure;
    }

  window_height_int = atoi (window_height_str);

  if (window_height_int <= 0)
    {
      C8_ERROR_WRITE (
          "failed to initialize chip8 emulator config, window height either "
          "is not a number greater than zero or not a number at all");

      goto c8_chip8_initialize_config_failure;
    }

  chip->window_height = (C8_SIZE)window_height_int;

  debug_x_str = c8_ini_get (ini, "c8_chip8_debug_text_x");

  if (debug_x_str == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_chip8_debug_text_x'");

      goto c8_chip8_initialize_config_failure;
    }

  debug_x_int = atoi (debug_x_str);

  if (debug_x_int <= 0)
    {
      C8_ERROR_WRITE (
          "failed to initialize chip8 emulator config, debug text x either is "
          "not a number greater than or equal to zero or not a number at all");

      goto c8_chip8_initialize_config_failure;
    }

  chip->debug_text_x = (C8_SIZE)debug_x_int;

  debug_y_str = c8_ini_get (ini, "c8_chip8_debug_text_y");

  if (debug_y_str == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_chip8_debug_text_y'");

      goto c8_chip8_initialize_config_failure;
    }

  debug_y_int = atoi (debug_y_str);

  if (debug_y_int <= 0)
    {
      C8_ERROR_WRITE (
          "failed to initialize chip8 emulator config, debug text y either is "
          "not a number greater than or equal to zero or not a number at all");

      goto c8_chip8_initialize_config_failure;
    }

  chip->debug_text_y = (C8_SIZE)debug_y_int;

  keyboard_0 = c8_ini_get (ini, "c8_keyboard_0");

  if (keyboard_0 == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_0'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_0) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_0);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_0, chip->keys[0x0]);

  keyboard_1 = c8_ini_get (ini, "c8_keyboard_1");

  if (keyboard_1 == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_1'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_1) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_1);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_1, chip->keys[0x1]);

  keyboard_2 = c8_ini_get (ini, "c8_keyboard_2");

  if (keyboard_2 == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_2'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_2) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_2);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_2, chip->keys[0x2]);

  keyboard_3 = c8_ini_get (ini, "c8_keyboard_3");

  if (keyboard_3 == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_3'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_3) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_3);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_3, chip->keys[0x3]);

  keyboard_4 = c8_ini_get (ini, "c8_keyboard_4");

  if (keyboard_4 == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_4'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_4) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_4);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_4, chip->keys[0x4]);

  keyboard_5 = c8_ini_get (ini, "c8_keyboard_5");

  if (keyboard_5 == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_5'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_5) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_5);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_5, chip->keys[0x5]);

  keyboard_6 = c8_ini_get (ini, "c8_keyboard_6");

  if (keyboard_6 == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_06'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_6) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_6);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_6, chip->keys[0x6]);

  keyboard_7 = c8_ini_get (ini, "c8_keyboard_7");

  if (keyboard_7 == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_7'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_7) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_7);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_7, chip->keys[0x7]);

  keyboard_8 = c8_ini_get (ini, "c8_keyboard_8");

  if (keyboard_8 == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_8'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_8) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_8);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_8, chip->keys[0x8]);

  keyboard_9 = c8_ini_get (ini, "c8_keyboard_9");

  if (keyboard_9 == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_9'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_9) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_9);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_9, chip->keys[0x9]);

  keyboard_A = c8_ini_get (ini, "c8_keyboard_A");

  if (keyboard_A == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_A'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_A) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_A);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_A, chip->keys[0xA]);

  keyboard_B = c8_ini_get (ini, "c8_keyboard_B");

  if (keyboard_B == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_B'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_B) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_B);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_B, chip->keys[0xB]);

  keyboard_C = c8_ini_get (ini, "c8_keyboard_C");

  if (keyboard_C == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_C'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_C) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_C);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_C, chip->keys[0xC]);

  keyboard_D = c8_ini_get (ini, "c8_keyboard_D");

  if (keyboard_D == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_D'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_D) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_D);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_D, chip->keys[0xD]);

  keyboard_E = c8_ini_get (ini, "c8_keyboard_E");

  if (keyboard_E == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_E'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_E) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_E);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_E, chip->keys[0xE]);

  keyboard_F = c8_ini_get (ini, "c8_keyboard_F");

  if (keyboard_F == NULL)
    {
      C8_ERROR_WRITE ("failed to initialize chip8 emulator config, unable to "
                      "locate property 'c8_keyboard_F'");

      goto c8_chip8_initialize_config_failure;
    }

  if (c8_events_valid_key (keyboard_F) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_F);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_F, chip->keys[0xF]);

  keyboard_debug = c8_ini_get (ini, "c8_keyboard_debug");

  if (c8_events_valid_key (keyboard_debug) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_debug);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_debug, chip->debug_key);

  keyboard_step = c8_ini_get (ini, "c8_keyboard_step");

  if (c8_events_valid_key (keyboard_step) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_step);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_step, chip->step_key);

  keyboard_quit = c8_ini_get (ini, "c8_keyboard_quit");

  if (c8_events_valid_key (keyboard_quit) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE_1 (
          "failed to initialize chip8 emulator config, invalid key '%s', must "
          "be 0-1, a-z, A-Z, TAB, RETURN, ESCAPE or SPACE.",
          keyboard_quit);

      goto c8_chip8_initialize_config_failure;
    }

  c8_chip8_keyboard_copy (keyboard_quit, chip->quit_key);

  c8_ini_destroy (&ini);

  return C8_BOOL_TRUE;

c8_chip8_initialize_config_failure:
  c8_ini_destroy (&ini);

  return C8_BOOL_FALSE;
}

C8_BOOL
c8_chip8_initialize (C8_CHIP8_CHIP *const chip)
{
  C8_SIZE index_1 = 0;

  C8_SIZE index_2 = 0;

  assert (chip != NULL);

  chip->graphics = NULL;

  chip->display = NULL;

  for (index_1 = 0; index_1 < C8_CHIP8_TEXT_ARRAY_SIZE; ++index_1)
    {
      chip->text[index_1] = NULL;
    }

  if (c8_chip8_initialize_config (chip) != C8_BOOL_TRUE)
    {
      C8_ERROR_WRITE (
          "failed to initialize chip8 emulator, loading of ini file failed");

      goto c8_chip8_initialize_failure;
    }

  c8_machine_initialize (&(chip->machine));

  chip->opcode = 0x0;

  chip->debug = C8_BOOL_FALSE;

  if (c8_machine_load_code (&(chip->machine), chip->rom_file) == C8_BOOL_FALSE)
    {
      C8_ERROR_WRITE (
          "failed to initialize chip8 emulator, loading of code failed");

      goto c8_chip8_initialize_failure;
    }

  c8_timer_initialize (&(chip->timer));

  chip->display = c8_display_create (
      chip->window_title, chip->window_width, chip->window_height,
      C8_MACHINE_GRAPHICS_WIDTH, C8_MACHINE_GRAPHICS_HEIGHT, chip->font_file,
      chip->font_size);

  if (chip->display == NULL)
    {
      C8_ERROR_WRITE (
          "failed to initialize chip8 emulator, creation of display failed");

      goto c8_chip8_initialize_failure;
    }

  chip->graphics = c8_display_render_create (chip->display, C8_BOOL_TRUE);

  if (chip->graphics == NULL)
    {
      C8_ERROR_WRITE (
          "failed to initialize chip8 emulator, creation of graphics "
          "render failed");

      goto c8_chip8_initialize_failure;
    }

  for (index_1 = 0; index_1 < C8_CHIP8_TEXT_ARRAY_SIZE; ++index_1)
    {
      chip->text[index_1]
          = c8_display_render_create (chip->display, C8_BOOL_FALSE);

      if (chip->text[index_1] == NULL)
        {
          C8_ERROR_WRITE (
              "failed to initalize chip8 emulator, creation of text "
              "render failed");

          goto c8_chip8_initialize_failure;
        }
    }

  return C8_BOOL_TRUE;

c8_chip8_initialize_failure:
  for (index_2 = 0; index_2 < C8_CHIP8_TEXT_ARRAY_SIZE; ++index_2)
    {
      if (chip->text[index_2] != NULL)
        {
          c8_display_render_destroy (chip->text[index_2]);

          chip->text[index_2] = NULL;
        }
    }

  if (chip->graphics != NULL)
    {
      c8_display_render_destroy (chip->graphics);

      chip->graphics = NULL;
    }

  if (chip->display != NULL)
    {
      c8_display_destroy (chip->display);

      chip->display = NULL;
    }

  return C8_BOOL_FALSE;
}

C8_BOOL
c8_chip8_process (C8_CHIP8_CHIP *const chip, C8_BOOL *running)
{
  C8_BOOL step = C8_BOOL_FALSE;

  assert (chip != NULL);

  assert (chip->display != NULL);

  assert (chip->graphics != NULL);

  assert (running != NULL);

c8_chip8_process_continue:
  c8_timer_process (&(chip->timer));

  if (chip->machine.program_counter > C8_MACHINE_PROGRAM_END_ADDRESS
      || c8_events_process (&(chip->machine), &(chip->debug), &step,
                            chip->keys, chip->debug_key, chip->step_key,
                            chip->quit_key)
             != C8_BOOL_TRUE)
    {
      *running = C8_BOOL_FALSE;

      return C8_BOOL_TRUE;
    }

  if (chip->debug != C8_BOOL_TRUE || step == C8_BOOL_TRUE)
    {
      chip->opcode = c8_machine_get_next_opcode (&(chip->machine));

      if (chip->opcode == 0x0)
        {
          *running = C8_BOOL_FALSE;

          return C8_BOOL_TRUE;
        }

      if (c8_machine_process_opcode (&(chip->machine), chip->opcode)
          != C8_BOOL_TRUE)
        {
          C8_ERROR_WRITE_1 ("failed to process chip8 emulator, processing of "
                            "opcode '%x' failed",
                            chip->opcode);

          return C8_BOOL_FALSE;
        }

      if (chip->machine.key_wait == C8_BOOL_TRUE)
        {
          goto c8_chip8_process_continue;
        }

      c8_machine_process_timers (&(chip->machine));

      if (chip->machine.sound_timer > 0)
        {
          c8_display_beep (chip->display, C8_BOOL_TRUE);
        }
      else
        {
          c8_display_beep (chip->display, C8_BOOL_FALSE);
        }
    }

  return C8_BOOL_TRUE;
}

C8_BOOL
c8_chip8_generate_text (C8_CHIP8_CHIP *const chip, const C8_SIZE x,
                        const C8_SIZE y)
{
  C8_SIZE index_1 = 1;

  C8_SIZE index_2 = 0;

  C8_SIZE yy = y;

  C8_CHAR buffers[C8_CHIP8_TEXT_ARRAY_SIZE][256];

  sprintf (buffers[0],
           "PC: 0x%4.4x, I: 0x%4.4x, DT: 0x%2.2x, ST: 0x%2.2x, KWAIT: 0x%2.2x",
           chip->machine.program_counter, chip->machine.index_register,
           chip->machine.delay_timer, chip->machine.sound_timer,
           chip->machine.key_wait);

  for (index_1 = 0; index_1 < C8_MACHINE_STACK_SIZE; ++index_1)
    {
      sprintf (buffers[index_1 + 1],
               "ST[0x%x] = 0x%4.4x, RG[0x%x] = 0x%2.2x, KS[0x%x] = 0x%2.2x",
               (C8_U32)index_1, chip->machine.stack[index_1], (C8_U32)index_1,
               chip->machine.registers[index_1], (C8_U32)index_1,
               chip->machine.key_state[index_1]);
    }

  sprintf (buffers[17], "OPC = 0x%4.4x, RG[0xF] = 0x%2.2x", chip->opcode,
           chip->machine.registers[16]);

  for (index_1 = 0; index_1 < C8_CHIP8_TEXT_ARRAY_SIZE; ++index_1)
    {
      C8_DISPLAY_DIMENSIONS dimensions;

      if (c8_display_render_text (chip->display, chip->text[index_1], x, yy,
                                  chip->font_color, buffers[index_1])
          != C8_BOOL_TRUE)
        {
          C8_ERROR_WRITE (
              "failed to render text, rendering of text to texture failed");

          for (index_2 = 0; index_2 < C8_CHIP8_TEXT_ARRAY_SIZE; ++index_2)
            {
              if (chip->text[index_2] != NULL)
                {
                  c8_display_render_destroy (chip->text[index_2]);

                  chip->text[index_2] = NULL;
                }
            }

          return C8_BOOL_FALSE;
        }

      c8_display_render_dimensions (chip->text[index_1], &dimensions);

      yy += dimensions.height + 1;
    }

  return C8_BOOL_TRUE;
}

C8_BOOL
c8_chip8_render (C8_CHIP8_CHIP *const chip)
{
  C8_SIZE index = 0;

  c8_timer_accumulate (&(chip->timer));

  if (c8_display_render_graphics (chip->graphics, chip->machine.graphics)
      != C8_BOOL_TRUE)
    {
      C8_ERROR_WRITE (
          "failed to render, rendering of graphics to texture failed");

      return C8_BOOL_FALSE;
    }

  if (c8_display_render (chip->display, chip->graphics) != C8_BOOL_TRUE)
    {
      C8_ERROR_WRITE (
          "failed to render, rendering graphics to display failed");

      return C8_BOOL_FALSE;
    }

  if (chip->debug == C8_BOOL_TRUE)
    {
      if (c8_chip8_generate_text (chip, chip->debug_text_x, chip->debug_text_y)
          != C8_BOOL_TRUE)
        {
          C8_ERROR_WRITE (
              "failed to render, rendering of text to texture failed");

          return C8_BOOL_FALSE;
        }

      for (index = 0; index < C8_CHIP8_TEXT_ARRAY_SIZE; ++index)
        {
          if (c8_display_render (chip->display, chip->text[index])
              != C8_BOOL_TRUE)
            {
              C8_ERROR_WRITE (
                  "failed to render, rendering text to display failed");

              return C8_BOOL_FALSE;
            }
        }
    }

  c8_display_update (chip->display);

  return C8_BOOL_TRUE;
}

C8_VOID
c8_chip8_terminate (C8_CHIP8_CHIP *const chip)
{
  C8_SIZE index = 0;

  assert (chip != NULL);

  for (index = 0; index < C8_CHIP8_TEXT_ARRAY_SIZE; ++index)
    {
      if (chip->text[index] != NULL)
        {
          c8_display_render_destroy (chip->text[index]);

          chip->text[index] = NULL;
        }
    }

  if (chip->graphics != NULL)
    {
      c8_display_render_destroy (chip->graphics);

      chip->graphics = NULL;
    }

  if (chip->display != NULL)
    {
      c8_display_destroy (chip->display);

      chip->display = NULL;
    }
}
