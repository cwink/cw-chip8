#include "../inc/c8_file.h"
#include "../inc/c8_error.h"

#include <assert.h>

C8_BOOL
c8_file_open_binary (FILE **file, const C8_CHAR *const filename)
{
  assert (file != NULL);

  assert (*file == NULL);

  *file = fopen (filename, "rb");

  C8_ERROR_CHECK_1 (*file != NULL, C8_BOOL_FALSE,
                    "failed to open binary file, open of file '%s' failed",
                    filename);

  return C8_BOOL_TRUE;
}

C8_BOOL
c8_file_close (FILE **file)
{
  assert (file != NULL);

  assert (*file != NULL);

  C8_ERROR_CHECK (fclose (*file) == 0, C8_BOOL_FALSE,
                  "failed to close file, file closing failed");

  *file = NULL;

  return C8_BOOL_TRUE;
}

C8_BOOL
c8_file_get (FILE *file, C8_S32 *value)
{
  assert (file != NULL);

  assert (value != NULL);

  clearerr (file);

  *value = fgetc (file);

  C8_ERROR_CHECK (ferror (file) == 0, C8_BOOL_FALSE,
                  "failed to get character from file, character get failed");

  return C8_BOOL_TRUE;
}
