#include "../inc/c8_error.h"

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define C8_ERROR_TIME_SIZE 64

C8_VOID
c8_error_write (const C8_CHAR *const filename, const C8_SIZE line,
                const C8_CHAR *const format, ...)
{
  va_list arguments;

  C8_CHAR buffer[C8_ERROR_MESSAGE_SIZE];

  C8_CHAR time_buffer[C8_ERROR_TIME_SIZE];

  struct tm *loc_time = NULL;

  time_t cur_time = 0;

  assert (filename != NULL);

  assert (format != NULL);

  cur_time = time (NULL);

  if (cur_time != -1)
    {
      loc_time = localtime (&cur_time);

      if (loc_time != NULL)
        {
          strftime (time_buffer, C8_ERROR_TIME_SIZE, "%Y-%m-%d %H:%M:%S",
                    loc_time);
        }
      else
        {
          strcpy (time_buffer, "UNKNOWN");
        }
    }
  else
    {
      strcpy (time_buffer, "UNKNOWN");
    }

  sprintf (buffer, "[%s][%s:%lu] %s\n", time_buffer, filename, line, format);

  va_start (arguments, format);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
  vfprintf (stderr, buffer, arguments);
#pragma GCC diagnostic pop

  va_end (arguments);
}
