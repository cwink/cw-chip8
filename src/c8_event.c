#include "../inc/c8_event.h"

#include <SDL2/SDL.h>

#include <assert.h>
#include <string.h>

static SDL_Scancode
c8_events_get_scancode (C8_CHAR key[32])
{
  if (strcmp (key, "0") == 0)
    {
      return SDL_SCANCODE_0;
    }

  if (strcmp (key, "1") == 0)
    {
      return SDL_SCANCODE_1;
    }

  if (strcmp (key, "2") == 0)
    {
      return SDL_SCANCODE_2;
    }

  if (strcmp (key, "3") == 0)
    {
      return SDL_SCANCODE_3;
    }

  if (strcmp (key, "4") == 0)
    {
      return SDL_SCANCODE_4;
    }

  if (strcmp (key, "5") == 0)
    {
      return SDL_SCANCODE_5;
    }

  if (strcmp (key, "6") == 0)
    {
      return SDL_SCANCODE_6;
    }

  if (strcmp (key, "7") == 0)
    {
      return SDL_SCANCODE_7;
    }

  if (strcmp (key, "8") == 0)
    {
      return SDL_SCANCODE_8;
    }

  if (strcmp (key, "9") == 0)
    {
      return SDL_SCANCODE_9;
    }

  if (strcmp (key, "A") == 0)
    {
      return SDL_SCANCODE_A;
    }

  if (strcmp (key, "B") == 0)
    {
      return SDL_SCANCODE_B;
    }

  if (strcmp (key, "C") == 0)
    {
      return SDL_SCANCODE_C;
    }

  if (strcmp (key, "D") == 0)
    {
      return SDL_SCANCODE_D;
    }

  if (strcmp (key, "E") == 0)
    {
      return SDL_SCANCODE_E;
    }

  if (strcmp (key, "F") == 0)
    {
      return SDL_SCANCODE_F;
    }

  if (strcmp (key, "G") == 0)
    {
      return SDL_SCANCODE_G;
    }

  if (strcmp (key, "H") == 0)
    {
      return SDL_SCANCODE_H;
    }

  if (strcmp (key, "I") == 0)
    {
      return SDL_SCANCODE_I;
    }

  if (strcmp (key, "J") == 0)
    {
      return SDL_SCANCODE_J;
    }

  if (strcmp (key, "K") == 0)
    {
      return SDL_SCANCODE_K;
    }

  if (strcmp (key, "L") == 0)
    {
      return SDL_SCANCODE_L;
    }

  if (strcmp (key, "M") == 0)
    {
      return SDL_SCANCODE_M;
    }

  if (strcmp (key, "N") == 0)
    {
      return SDL_SCANCODE_N;
    }

  if (strcmp (key, "O") == 0)
    {
      return SDL_SCANCODE_O;
    }

  if (strcmp (key, "P") == 0)
    {
      return SDL_SCANCODE_P;
    }

  if (strcmp (key, "Q") == 0)
    {
      return SDL_SCANCODE_Q;
    }

  if (strcmp (key, "R") == 0)
    {
      return SDL_SCANCODE_R;
    }

  if (strcmp (key, "S") == 0)
    {
      return SDL_SCANCODE_S;
    }

  if (strcmp (key, "T") == 0)
    {
      return SDL_SCANCODE_T;
    }

  if (strcmp (key, "U") == 0)
    {
      return SDL_SCANCODE_U;
    }

  if (strcmp (key, "V") == 0)
    {
      return SDL_SCANCODE_V;
    }

  if (strcmp (key, "W") == 0)
    {
      return SDL_SCANCODE_W;
    }

  if (strcmp (key, "X") == 0)
    {
      return SDL_SCANCODE_X;
    }

  if (strcmp (key, "Y") == 0)
    {
      return SDL_SCANCODE_Y;
    }

  if (strcmp (key, "Z") == 0)
    {
      return SDL_SCANCODE_Z;
    }

  if (strcmp (key, "TAB") == 0)
    {
      return SDL_SCANCODE_TAB;
    }

  if (strcmp (key, "RETURN") == 0)
    {
      return SDL_SCANCODE_RETURN;
    }

  if (strcmp (key, "SPACE") == 0)
    {
      return SDL_SCANCODE_SPACE;
    }

  if (strcmp (key, "ESCAPE") == 0)
    {
      return SDL_SCANCODE_ESCAPE;
    }

  assert (0);
}

C8_BOOL
c8_events_process (C8_MACHINE *machine, C8_BOOL *debug, C8_BOOL *step,
                   C8_CHAR keys[C8_MACHINE_KEY_SIZE][32],
                   C8_CHAR debug_key[32], C8_CHAR step_key[32],
                   C8_CHAR quit_key[32])
{
  SDL_Event event;

  C8_SIZE index = 0;

  assert (machine != NULL);

  assert (debug != NULL);

  assert (step != NULL);

  while (SDL_PollEvent (&event))
    {
      switch (event.type)
        {
        case SDL_WINDOWEVENT:
          switch (event.window.event)
            {
            case SDL_WINDOWEVENT_CLOSE:
              return C8_BOOL_FALSE;
            default:

              break;
            }

          break;
        default:

          break;
        }
    }

  for (index = 0x0; index < C8_MACHINE_KEY_SIZE; ++index)
    {
      if (SDL_GetKeyboardState (NULL)[c8_events_get_scancode (keys[index])]
          == SDL_TRUE)
        {
          machine->key_state[index] = C8_BOOL_TRUE;
        }
    }

  if (SDL_GetKeyboardState (NULL)[c8_events_get_scancode (debug_key)]
      == SDL_TRUE)
    {
      *debug = *debug == C8_BOOL_TRUE ? C8_BOOL_FALSE : C8_BOOL_TRUE;
    }

  if (SDL_GetKeyboardState (NULL)[c8_events_get_scancode (step_key)]
      == SDL_TRUE)
    {
      *step = C8_BOOL_TRUE;
    }

  if (SDL_GetKeyboardState (NULL)[c8_events_get_scancode (quit_key)]
      == SDL_TRUE)
    {
      return C8_BOOL_FALSE;
    }

  return C8_BOOL_TRUE;
}

C8_VOID
c8_events_reset (C8_MACHINE *machine)
{
  C8_SIZE index = 0;

  assert (machine != NULL);

  for (index = 0; index < C8_MACHINE_KEY_SIZE; ++index)
    {
      machine->key_state[index] = C8_BOOL_FALSE;
    }
}

C8_BOOL
c8_events_valid_key (const C8_CHAR *const key_string)
{
  C8_SIZE index = 0;

  C8_CHAR buffer[32];

  assert (key_string != NULL);

  for (index = 0; index < 32; ++index)
    {
      if (key_string[index] == '\0')
        {
          buffer[index] = '\0';

          break;
        }

      buffer[index] = (C8_CHAR)toupper (key_string[index]);
    }

  if (strcmp (buffer, "0") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "1") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "2") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "3") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "4") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "5") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "6") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "7") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "8") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "9") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "A") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "B") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "C") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "D") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "E") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "F") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "RETURN") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "TAB") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "SPACE") == 0)
    {
      return C8_BOOL_TRUE;
    }

  if (strcmp (buffer, "ESCAPE") == 0)
    {
      return C8_BOOL_TRUE;
    }

  return C8_BOOL_FALSE;
}
