#include "../inc/c8_machine.h"
#include "../inc/c8_error.h"
#include "../inc/c8_file.h"

#include <SDL2/SDL.h>

#include <assert.h>
#include <time.h>

static const C8_BYTE c8_font_set[C8_MACHINE_FONT_SET_SIZE]
    = { 0xF0, 0x90, 0x90, 0x90, 0xF0, 0x20, 0x60, 0x20, 0x20, 0x70, 0xF0, 0x10,
        0xF0, 0x80, 0xF0, 0xF0, 0x10, 0xF0, 0x10, 0xF0, 0x90, 0x90, 0xF0, 0x10,
        0x10, 0xF0, 0x80, 0xF0, 0x10, 0xF0, 0xF0, 0x80, 0xF0, 0x90, 0xF0, 0xF0,
        0x10, 0x20, 0x40, 0x40, 0xF0, 0x90, 0xF0, 0x90, 0xF0, 0xF0, 0x90, 0xF0,
        0x10, 0xF0, 0xF0, 0x90, 0xF0, 0x90, 0x90, 0xE0, 0x90, 0xE0, 0x90, 0xE0,
        0xF0, 0x80, 0x80, 0x80, 0xF0, 0xE0, 0x90, 0x90, 0x90, 0xE0, 0xF0, 0x80,
        0xF0, 0x80, 0xF0, 0xF0, 0x80, 0xF0, 0x80, 0x80 };

static C8_BYTE c8_machine_bit_table[8]
    = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };

static C8_BOOL
c8_machine_process_0NNN (C8_MACHINE *machine)
{
  assert (machine != NULL);

  C8_ERROR_WRITE ("failed to call routine, functionality not implemented");

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_00E0 (C8_MACHINE *machine)
{
  C8_SIZE index = 0;

  assert (machine != NULL);

  for (index = 0; index < C8_MACHINE_GRAPHICS_SIZE; ++index)
    {
      machine->graphics[index] = C8_MACHINE_RGB_OFF;
    }

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_00EE (C8_MACHINE *machine)
{
  C8_U16 program_counter = 0;

  assert (machine != NULL);

  C8_ERROR_CHECK (
      machine->stack_pointer < C8_MACHINE_STACK_SIZE, C8_BOOL_FALSE,
      "failed to return from subroutine, stack pointer is too large");

  C8_ERROR_CHECK (machine->stack_pointer != 0, C8_BOOL_FALSE,
                  "failed to return from subroutine, stack pointer is zero");

  program_counter = machine->stack[machine->stack_pointer];

  C8_ERROR_CHECK_1 (program_counter <= C8_MACHINE_PROGRAM_END_ADDRESS,
                    C8_BOOL_FALSE,
                    "failed to return from subroutine, calculated program "
                    "address '%x' is too large",
                    program_counter);

  machine->program_counter = program_counter;

  --machine->stack_pointer;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_1NNN (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U16 nnn = opcode & 0x0FFF;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (
      nnn <= C8_MACHINE_PROGRAM_END_ADDRESS, C8_BOOL_FALSE,
      "failed to jump to address, program address '%x' is too large", nnn);

  machine->program_counter = nnn;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_2NNN (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U16 nnn = opcode & 0x0FFF;

  assert (machine != NULL);

  C8_ERROR_CHECK (machine->stack_pointer + 1 < C8_MACHINE_STACK_SIZE,
                  C8_BOOL_FALSE,
                  "failed to call subroutine, stack pointer is too large");

  C8_ERROR_CHECK_1 (
      nnn <= C8_MACHINE_PROGRAM_END_ADDRESS, C8_BOOL_FALSE,
      "failed to call subroutine, program address '%x' is too large", nnn);

  ++machine->stack_pointer;

  machine->stack[machine->stack_pointer] = machine->program_counter;

  machine->program_counter = nnn;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_3XNN (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 nn = opcode & 0x00FF;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to check if register is equal to value for skip, "
                    "invalid register '%x'",
                    x);

  if (machine->registers[x] == nn)
    {
      machine->program_counter += 2;
    }

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_4XNN (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 nn = opcode & 0x00FF;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to check if register is not equal to value for "
                    "skip, invalid register '%x'",
                    x);

  if (machine->registers[x] != nn)
    {
      machine->program_counter += 2;
    }

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_5XY0 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to check if register is equal to register for "
                    "skip, invalid register '%x'",
                    x);

  C8_ERROR_CHECK_1 (y < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to check if register is equal to register for "
                    "skip, invalid register '%x'",
                    y);

  if (machine->registers[x] == machine->registers[y])
    {
      machine->program_counter += 2;
    }

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_6XNN (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 nn = opcode & 0x00FF;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to set register to value, invalid register '%x'",
                    x);

  machine->registers[x] = nn;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_7XNN (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 nn = opcode & 0x00FF;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to add value to register, invalid register '%x'",
                    x);

  machine->registers[x] += nn;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_8XY0 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (
      x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to set register to another register, invalid register '%x'", x);

  C8_ERROR_CHECK_1 (
      y < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to set register to another register, invalid register '%x'", y);

  machine->registers[x] = machine->registers[y];

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_8XY1 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (
      x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to logical 'or' two registers, invalid register '%x'", x);

  C8_ERROR_CHECK_1 (
      y < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to logical 'or' two registers, invalid register '%x'", y);

  machine->registers[x] |= machine->registers[y];

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_8XY2 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (
      x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to logical 'and' two registers, invalid register '%x'", x);

  C8_ERROR_CHECK_1 (
      y < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to logical 'and' two registers, invalid register '%x'", y);

  machine->registers[x] &= machine->registers[y];

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_8XY3 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (
      x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to logical 'xor' two registers, invalid register '%x'", x);

  C8_ERROR_CHECK_1 (
      y < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to logical 'xor' two registers, invalid register '%x'", y);

  machine->registers[x] ^= machine->registers[y];

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_8XY4 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  C8_U16 value = 0;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to add two registers, invalid register '%x'", x);

  C8_ERROR_CHECK_1 (y < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to add two registers, invalid register '%x'", y);

  value = (C8_U16)machine->registers[x] + (C8_U16)machine->registers[y];

  machine->registers[x] = (C8_U8) (value & 0xFF);

  machine->registers[0xF] = (value > 0xFF);

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_8XY5 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to subtract two registers, invalid register '%x'",
                    x);

  C8_ERROR_CHECK_1 (y < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to subtract two registers, invalid register '%x'",
                    y);

  machine->registers[0xF] = (machine->registers[x] > machine->registers[y]);

  machine->registers[x] -= machine->registers[y];

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_8XY6 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to shift register right, invalid register '%x'",
                    x);

  C8_ERROR_CHECK_1 (y < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to shift register right, invalid register '%x'",
                    y);

  machine->registers[0xF] = (machine->registers[x] & 0x1);

  machine->registers[x] >>= 1;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_8XY7 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (
      x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to inverse subtract two registers, invalid register '%x'", x);

  C8_ERROR_CHECK_1 (
      y < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to inverse subtract two registers, invalid register '%x'", y);

  machine->registers[0xF] = (machine->registers[x] < machine->registers[y]);

  machine->registers[x] = machine->registers[y] - machine->registers[x];

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_8XYE (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to shift register left, invalid register '%x'", x);

  C8_ERROR_CHECK_1 (y < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to shift register left, invalid register '%x'", y);

  machine->registers[0xF] = (machine->registers[x] & 0x80) >> 7;

  machine->registers[x] <<= 1;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_9XY0 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  assert (machine != NULL);

  C8_ERROR_CHECK_2 (x < C8_MACHINE_REGISTER_SIZE
                        && y < C8_MACHINE_REGISTER_SIZE,
                    C8_BOOL_FALSE,
                    "failed to check if register is not equal to register for "
                    "skip, invalid register '%x' or '%x'",
                    x, y);

  if (machine->registers[x] != machine->registers[y])
    {
      machine->program_counter += 2;
    }

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_ANNN (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U16 index = opcode & 0x0FFF;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (index <= C8_MACHINE_PROGRAM_END_ADDRESS, C8_BOOL_FALSE,
                    "failed to set index to address, invalid index '%x'",
                    index);

  machine->index_register = index;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_BNNN (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U16 program_counter = opcode & 0x0FFF;

  assert (machine != NULL);

  program_counter += (C8_U16)machine->registers[0x0];

  C8_ERROR_CHECK_1 (
      program_counter <= C8_MACHINE_PROGRAM_END_ADDRESS, C8_BOOL_FALSE,
      "failed to jump to location address, invalid location address '%x'",
      program_counter);

  machine->program_counter = program_counter;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_CXNN (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 nn = opcode & 0x00FF;

  C8_S32 random = 0;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (
      x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to 'and' a random 8-bit value to value, invalid address '%x'",
      x);

  srand ((C8_U32)SDL_GetTicks ());

  random = rand ();

  machine->registers[x]
      = (C8_U8) (((C8_R64)random / ((C8_R64)RAND_MAX + 1.0)) * 256) & nn;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_DXYN (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U8 y = (opcode & 0x00F0) >> 4;

  C8_U8 n = opcode & 0x000F;

  C8_SIZE yy = 0;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to draw to graphics memory, invalid address '%x'",
                    x);

  C8_ERROR_CHECK_1 (y < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to draw to graphics memory, invalid address '%x'",
                    y);

  C8_ERROR_CHECK (
      machine->index_register + n < C8_MACHINE_PROGRAM_END_ADDRESS,
      C8_BOOL_FALSE,
      "failed to draw to graphics memory, index register too large");

  machine->registers[0xF] = 0x0;

  for (yy = 0; yy < n; ++yy)
    {
      C8_S8 b = 0;

      C8_U8 pixel = machine->memory[machine->index_register + yy];

      for (b = 7; b >= 0; --b)
        {
          C8_U32 value
              = ((c8_machine_bit_table[(C8_SIZE)b] & pixel) >> b) == 0x1
                    ? C8_MACHINE_RGB_ON
                    : C8_MACHINE_RGB_OFF;

          C8_SIZE xx = machine->registers[x] + (7 - (C8_SIZE)b);

          C8_SIZE yyy = machine->registers[y] + yy;

          C8_SIZE index = 0;

          while (xx >= C8_MACHINE_GRAPHICS_WIDTH)
            {
              xx -= C8_MACHINE_GRAPHICS_WIDTH;
            }

          while (yyy >= C8_MACHINE_GRAPHICS_HEIGHT)
            {
              yyy -= C8_MACHINE_GRAPHICS_HEIGHT;
            }

          index = (yyy * C8_MACHINE_GRAPHICS_WIDTH) + xx;

          if (machine->graphics[index] != value)
            {
              machine->graphics[index] = value;

              if (value == C8_MACHINE_RGB_OFF)
                {
                  machine->registers[0xF] = 0x1;
                }
            }
        }
    }

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_EX9E (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (
      x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to check key to skip next instruction, invalid register '%x'",
      x);

  C8_ERROR_CHECK_1 (
      machine->registers[x] < C8_MACHINE_KEY_SIZE, C8_BOOL_FALSE,
      "failed to check key to skip next instruction, invalid key '%x'",
      machine->registers[x]);

  if (machine->key_state[machine->registers[x]] == C8_BOOL_TRUE)
    {
      machine->program_counter += 2;
    }

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_EXA1 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to check key to not skip next instruction, "
                    "invalid register '%x'",
                    x);

  C8_ERROR_CHECK_1 (
      machine->registers[x] < C8_MACHINE_KEY_SIZE, C8_BOOL_FALSE,
      "failed to check key to not skip next instruction, invalid key '%x'",
      machine->registers[x]);

  if (machine->key_state[machine->registers[x]] != C8_BOOL_TRUE)
    {
      machine->program_counter += 2;
    }

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_FX07 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to get delay timer, invalid register '%x'", x);

  machine->registers[x] = machine->delay_timer;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_FX0A (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_SIZE index = 0;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to wait and get key, invalid register '%x'", x);

  machine->key_wait = C8_BOOL_TRUE;

  for (index = 0; index < C8_MACHINE_KEY_SIZE; ++index)
    {
      if (machine->key_state[index] == C8_BOOL_TRUE)
        {
          machine->key_wait = C8_BOOL_FALSE;

          machine->registers[x] = (C8_U8)index;

          machine->program_counter += 2;
        }
    }

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_FX15 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to set delay timer, invalid register '%x'", x);

  machine->delay_timer = machine->registers[x];

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_FX18 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to set sound timer, invalid register '%x'", x);

  machine->sound_timer = machine->registers[x];

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_FX1E (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_U16 index_register = 0;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (
      x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to set index to register value, invalid register '%x'", x);

  index_register = machine->index_register + machine->registers[x];

  C8_ERROR_CHECK_1 (
      index_register <= C8_MACHINE_PROGRAM_END_ADDRESS, C8_BOOL_FALSE,
      "failed to set index to register value, index value '%x' is too large",
      index_register);

  machine->index_register = index_register;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_FX29 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (
      x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
      "failed to set index to font address, invalid register '%x'", x);

  machine->index_register = machine->registers[x] * 5;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_FX33 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to set index to BCD, invalid register '%x'", x);

  C8_ERROR_CHECK_1 (
      machine->index_register + 2 <= C8_MACHINE_PROGRAM_END_ADDRESS,
      C8_BOOL_FALSE,
      "failed to set index to BCD, index value '%x' is too large",
      machine->index_register);

  machine->memory[machine->index_register] = machine->registers[x] / 100;

  machine->memory[machine->index_register + 1]
      = (machine->registers[x] / 10) % 10;

  machine->memory[machine->index_register + 2] = machine->registers[x] % 10;

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_FX55 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_SIZE index = 0;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to register dump, invalid register '%x'", x);

  C8_ERROR_CHECK_1 (
      machine->index_register + x + 1 <= C8_MACHINE_PROGRAM_END_ADDRESS,
      C8_BOOL_FALSE, "failed to register dump, index value '%x' is too large",
      machine->index_register);

  for (index = 0; index <= x; ++index)
    {
      machine->memory[machine->index_register + index]
          = machine->registers[index];
    }

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

static C8_BOOL
c8_machine_process_FX65 (C8_MACHINE *machine, const C8_U16 opcode)
{
  C8_U8 x = (opcode & 0x0F00) >> 8;

  C8_SIZE index = 0;

  assert (machine != NULL);

  C8_ERROR_CHECK_1 (x < C8_MACHINE_REGISTER_SIZE, C8_BOOL_FALSE,
                    "failed to register load, invalid register '%x'", x);

  C8_ERROR_CHECK_1 (
      machine->index_register + x + 1 <= C8_MACHINE_PROGRAM_END_ADDRESS,
      C8_BOOL_FALSE, "failed to register load, index value '%x' is too large",
      machine->index_register);

  for (index = 0; index <= x; ++index)
    {
      machine->registers[index]
          = machine->memory[machine->index_register + index];
    }

  machine->program_counter += 2;

  return C8_BOOL_TRUE;
}

C8_VOID
c8_machine_initialize (C8_MACHINE *machine)
{
  C8_SIZE index = 0;

  assert (machine != NULL);

  for (index = 0; index < C8_MACHINE_MEMORY_SIZE; ++index)
    {
      machine->memory[index] = 0x0;
    }

  for (index = C8_MACHINE_FONT_START_ADDRESS; index < C8_MACHINE_FONT_SET_SIZE;
       ++index)
    {
      machine->memory[index] = c8_font_set[index];
    }

  for (index = 0; index < C8_MACHINE_REGISTER_SIZE; ++index)
    {
      machine->registers[index] = 0x0;
    }

  machine->index_register = 0x00;

  machine->program_counter = C8_MACHINE_PROGRAM_START_ADDRESS;

  for (index = 0; index < C8_MACHINE_GRAPHICS_SIZE; ++index)
    {
      machine->graphics[index] = C8_MACHINE_RGB_OFF;
    }

  machine->delay_timer = 0x0;

  machine->sound_timer = 0x0;

  for (index = 0; index < C8_MACHINE_STACK_SIZE; ++index)
    {
      machine->stack[index] = 0x00;
    }

  machine->stack_pointer = 0x00;

  for (index = 0; index < C8_MACHINE_KEY_SIZE; ++index)
    {
      machine->key_state[index] = C8_BOOL_FALSE;
    }

  machine->key_wait = C8_BOOL_FALSE;
}

C8_BOOL
c8_machine_load_code (C8_MACHINE *machine, const C8_CHAR *const filename)
{
  C8_SIZE index = 0;

  C8_S32 value = 0;

  FILE *file = NULL;

  assert (machine != NULL);

  assert (filename != NULL);

  C8_ERROR_CHECK_1 (
      c8_file_open_binary (&file, filename) == C8_BOOL_TRUE, C8_BOOL_FALSE,
      "failed to load machine code, reading of file '%s' failed", filename);

  for (index = C8_MACHINE_PROGRAM_START_ADDRESS;
       index < C8_MACHINE_PROGRAM_END_ADDRESS; ++index)
    {
      if (c8_file_get (file, &value) != C8_BOOL_TRUE)
        {
          C8_ERROR_WRITE_1 ("failed to load machine code, reading character "
                            "from file '%s' failed",
                            filename);

          C8_ERROR_CHECK_1 (
              c8_file_close (&file) == C8_BOOL_TRUE, C8_BOOL_FALSE,
              "failed to load machine code, closing of file '%s' failed",
              filename);

          return C8_BOOL_FALSE;
        }

      if (value == EOF)
        {
          break;
        }

      machine->memory[index] = (C8_BYTE)value;
    }

  if (value != EOF)
    {
      C8_ERROR_WRITE_1 (
          "failed to load machine code, did not reach EOF for file '%s'",
          filename);

      C8_ERROR_CHECK_1 (
          c8_file_close (&file) == C8_BOOL_TRUE, C8_BOOL_FALSE,
          "failed to load machine code, closing of file '%s' failed",
          filename);

      return C8_BOOL_FALSE;
    }

  C8_ERROR_CHECK_1 (c8_file_close (&file) == C8_BOOL_TRUE, C8_BOOL_FALSE,
                    "failed to load machine code, closing of file '%s' failed",
                    filename);

  return C8_BOOL_TRUE;
}

C8_OPCODE
c8_machine_get_next_opcode (C8_MACHINE *machine)
{
  C8_OPCODE opcode = 0x0000;

  assert (machine != NULL);

  opcode = (C8_U16) (machine->memory[machine->program_counter] << 8)
           | (C8_U16)machine->memory[machine->program_counter + 1];

  return opcode;
}

C8_VOID
c8_machine_process_timers (C8_MACHINE *machine)
{
  assert (machine != NULL);

  if (machine->delay_timer >= 1)
    {
      --machine->delay_timer;
    }

  if (machine->sound_timer >= 1)
    {
      --machine->sound_timer;
    }
}

C8_BOOL
c8_machine_process_opcode (C8_MACHINE *machine, const C8_OPCODE opcode)
{
  C8_BOOL result = C8_BOOL_FALSE;

  assert (machine != NULL);

  C8_ERROR_CHECK (machine->program_counter <= C8_MACHINE_PROGRAM_END_ADDRESS,
                  C8_BOOL_FALSE,
                  "failed to process opcode, reached end of program");

  switch ((opcode & 0xF000) >> 12)
    {
    case 0x0:
      switch ((opcode & 0x0F00) >> 8)
        {
        case 0x0:
          switch ((opcode & 0x00F0) >> 4)
            {
            case 0xE:
              switch (opcode & 0x000F)
                {
                case 0x0:
                  result = c8_machine_process_00E0 (machine);

                  break;
                case 0xE:
                  result = c8_machine_process_00EE (machine);

                  break;

                default:
                  break;
                }

              break;

            default:
              result = c8_machine_process_0NNN (machine);

              break;
            }

          break;
        default:

          break;
        }

      break;
    case 0x1:
      result = c8_machine_process_1NNN (machine, opcode);

      break;
    case 0x2:
      result = c8_machine_process_2NNN (machine, opcode);

      break;
    case 0x3:
      result = c8_machine_process_3XNN (machine, opcode);

      break;
    case 0x4:
      result = c8_machine_process_4XNN (machine, opcode);

      break;
    case 0x5:
      switch (opcode & 0x000F)
        {
        case 0x0:
          result = c8_machine_process_5XY0 (machine, opcode);

          break;
        default:

          break;
        }

      break;
    case 0x6:
      result = c8_machine_process_6XNN (machine, opcode);

      break;
    case 0x7:
      result = c8_machine_process_7XNN (machine, opcode);

      break;
    case 0x8:
      switch (opcode & 0x000F)
        {
        case 0x0:
          result = c8_machine_process_8XY0 (machine, opcode);

          break;
        case 0x1:
          result = c8_machine_process_8XY1 (machine, opcode);

          break;
        case 0x2:
          result = c8_machine_process_8XY2 (machine, opcode);

          break;
        case 0x3:
          result = c8_machine_process_8XY3 (machine, opcode);

          break;
        case 0x4:
          result = c8_machine_process_8XY4 (machine, opcode);

          break;
        case 0x5:
          result = c8_machine_process_8XY5 (machine, opcode);

          break;
        case 0x6:
          result = c8_machine_process_8XY6 (machine, opcode);

          break;
        case 0x7:
          result = c8_machine_process_8XY7 (machine, opcode);

          break;
        case 0xE:
          result = c8_machine_process_8XYE (machine, opcode);

          break;
        default:

          break;
        }

      break;

    case 0x9:
      switch (opcode & 0x000F)
        {
        case 0x0:
          result = c8_machine_process_9XY0 (machine, opcode);

          break;
        default:

          break;
        }

      break;
    case 0xA:
      result = c8_machine_process_ANNN (machine, opcode);

      break;
    case 0xB:
      result = c8_machine_process_BNNN (machine, opcode);

      break;
    case 0xC:
      result = c8_machine_process_CXNN (machine, opcode);

      break;
    case 0xD:
      result = c8_machine_process_DXYN (machine, opcode);

      break;
    case 0xE:
      switch ((opcode & 0x00F0) >> 4)
        {
        case 0x9:
          switch (opcode & 0x000F)
            {
            case 0xE:
              result = c8_machine_process_EX9E (machine, opcode);

              break;
            default:

              break;
            }

          break;
        case 0xA:
          switch (opcode & 0x000F)
            {
            case 0x1:
              result = c8_machine_process_EXA1 (machine, opcode);

              break;
            default:

              break;
            }

          break;
        default:

          break;
        }

      break;
    case 0xF:
      switch ((opcode & 0x00F0) >> 4)
        {
        case 0x0:
          switch (opcode & 0x000F)
            {
            case 0x7:
              result = c8_machine_process_FX07 (machine, opcode);

              break;
            case 0xA:
              result = c8_machine_process_FX0A (machine, opcode);

              break;
            default:
              break;
            }

          break;
        case 0x1:
          switch (opcode & 0x000F)
            {
            case 0x5:
              result = c8_machine_process_FX15 (machine, opcode);

              break;
            case 0x8:
              result = c8_machine_process_FX18 (machine, opcode);

              break;
            case 0xE:
              result = c8_machine_process_FX1E (machine, opcode);

              break;
            default:
              break;
            }

          break;
        case 0x2:
          switch (opcode & 0x000F)
            {
            case 0x9:
              result = c8_machine_process_FX29 (machine, opcode);

              break;
            default:
              break;
            }

          break;
        case 0x3:
          switch (opcode & 0x000F)
            {
            case 0x3:
              result = c8_machine_process_FX33 (machine, opcode);

              break;
            default:
              break;
            }

          break;
        case 0x5:
          switch (opcode & 0x000F)
            {
            case 0x5:
              result = c8_machine_process_FX55 (machine, opcode);

              break;
            default:
              break;
            }

          break;
        case 0x6:
          switch (opcode & 0x000F)
            {
            case 0x5:
              result = c8_machine_process_FX65 (machine, opcode);

              break;
            default:
              break;
            }

          break;
        default:

          break;
        }

      break;
    default:

      break;
    }

  C8_ERROR_CHECK_2 (
      result == C8_BOOL_TRUE, C8_BOOL_FALSE,
      "failed to process opcode, opcode '%x' failed at address '%x'", opcode,
      machine->program_counter);

  return result;
}
